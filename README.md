# Data-Stories

This is an open source project developing a collection of functions that will generate stunning images from data that has been prepared for the purpose from raw data sets relating to climate change. Data wrangled with Python, visualised with JS

Contributions to this project will be enabled after the set up phase, due to complete by 1st August 2021.
